package com.example.sandbox.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.sandbox.model.Student;
import com.example.sandbox.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;

	/**
	 * 生徒全件取得
	 * @return 生徒リスト
	 */
	@Transactional(readOnly = true)
	public List<Student> getStudents() {
		return studentRepository.findAll();
	}

	/**
	 * 生徒の削除を行う
	 * @param students 生徒リスト
	 */
	@Transactional
	public void deleteStudents(List<Student> students) {
		studentRepository.deleteAll(students);
	}
}
