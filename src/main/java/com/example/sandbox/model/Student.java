package com.example.sandbox.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student implements Serializable, Comparable<Student> {
	@Id
	@GeneratedValue
	private Integer id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Student o) {
		return this.getId() - o.getId();
	}

	@Override
	public int hashCode() {
		return Integer.hashCode(this.getId());
	}
}
