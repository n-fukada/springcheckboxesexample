package com.example.sandbox.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentsForm implements Serializable {

	private Map<Student, Boolean> checked = new HashMap<>();

	public Map<Student, Boolean> getChecked() {
		return checked;
	}

	public void setChecked(Map<Student, Boolean> checked) {
		this.checked = checked;
	}

	/**
	 * checkboxパラメータの初期化を行う
	 * @param students 生徒リスト
	 */
	public void initChecked(List<Student> students) {
		students.forEach(student ->
			checked.put(student, false)
		);
	}

	/**
	 * 選択された生徒を取得する
	 * @return 生徒リスト
	 */
	public List<Student> getCheckedStudents() {
		List<Student> students = new ArrayList<>();
		getChecked().forEach((key, value) -> {
			if (value != null && value.equals(true)) {
				students.add(key);
			}
		});
		Collections.sort(students);
		return students;
	}
}
