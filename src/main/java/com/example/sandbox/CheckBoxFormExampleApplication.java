package com.example.sandbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckBoxFormExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckBoxFormExampleApplication.class, args);
	}
}
