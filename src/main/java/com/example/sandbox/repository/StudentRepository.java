package com.example.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sandbox.model.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {

}
