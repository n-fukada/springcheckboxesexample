package com.example.sandbox.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.sandbox.model.Student;
import com.example.sandbox.model.StudentsForm;
import com.example.sandbox.service.StudentService;

@Controller
public class MainController {

	@Autowired
	private StudentService studentService;

	// 生徒一覧画面
	@RequestMapping("/")
	String main(Model model) {
		// 生徒リスト取得
		List<Student> students = studentService.getStudents();
		model.addAttribute("students", students);
		// Form新規作成
		StudentsForm form = new StudentsForm();
		// checkboxパラメータ初期化
		form.initChecked(students);
		model.addAttribute("form", form);
		return "index";
	}

	// 削除確認画面
	@PostMapping("/confirm")
	String confirm(StudentsForm form, Model model) {
		if (form.getChecked().containsValue(true)) {
			// 選択された生徒のリスト取得
			model.addAttribute("students", form.getCheckedStudents());
			model.addAttribute("form", form);
			return "confirm";
		}
		return "redirect:/";
	}

	// 削除確認画面から「削除」ボタン押下
	@PostMapping(value = "/delete", params = "submit")
	String delete(StudentsForm form) {
		// 生徒の削除
		studentService.deleteStudents(form.getCheckedStudents());
		return "redirect:/";
	}

	// 削除確認画面から「戻る」ボタン押下
	@PostMapping(value = "/delete", params = "return")
	String goToIndex() {
		return "redirect:/";
	}
}
