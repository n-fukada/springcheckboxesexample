create table if not exists student (
	id int primary key auto_increment,
	name varchar(50)
);
insert into student (id, name) values
	(1, 'Asao'),
	(3, 'Ueno'),
	(4, 'Esumi'),
	(5, 'Okayama'),
	(7, 'Kinoshita'),
	(8, 'Kudou'),
	(11, 'Sano'),
	(12, 'Shimizu'),
	(13, 'Suzuki'),
	(15, 'Someya');
